public with sharing class FillContactFavoriteMascot
{
  IUserRepository userRepository;
  List<Contact> contacts = new List<Contact>();
  Id userId;

  public FillContactFavoriteMascot(IUserRepository userRepository, List<Contact> contacts, Id userId)
  {
    this.userRepository = userRepository;
    this.contacts = contacts;
    this.userId = userId;
  }

  public void handle()
  {
    User user = userRepository.getUser(userId);

    if (!user.Profile.PermissionsCustomizeApplication)
    {
      for(Contact con : contacts)
      {
        con = new ContactBuilder(con)
        	  .withFavouriteMascotAstro()
            .build();
      }
    }
  }
}
