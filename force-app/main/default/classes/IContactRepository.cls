public interface IContactRepository
{
  Contact getContact(Id contactId);
  void insertContacts(List<Contact> contacts);
  void updateContacts(List<Contact> contacts);
}