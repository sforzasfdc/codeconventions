public with sharing class UserRepository implements IUserRepository
{
  public User getUser(Id userId)
  {
    return[
        SELECT Profile.Name, Profile.PermissionsCustomizeApplication
        FROM User
        WHERE Id = :userId
    ];
  }
}