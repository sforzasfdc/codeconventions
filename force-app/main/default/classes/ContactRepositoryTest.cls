@IsTest
private class ContactRepositoryTest
{
  @IsTest
  static void getContact()
  {
    // Arrange
    Contact contactToInsert = new Contact(
        lastName = 'bar'
    );
    insert contactToInsert;

    ContactRepository contactRepository = new ContactRepository();

    // Act
    Contact contactToReturn = contactRepository.getContact(contactToInsert.Id);

    // Assert
    System.assertEquals(contactToInsert.Id, contactToReturn.Id);
  }

  @IsTest
  static void insertContacts()
  {
    // Arrange
    Contact contactToInsert = new Contact(
        lastName = 'bar'
    );

    ContactRepository contactRepository = new ContactRepository();

    // Act
    contactRepository.insertContacts(new List<Contact>{contactToInsert});

    // Assert
    System.assertNotEquals(null, contactToInsert.Id);
  }

  @IsTest
  static void updateContacts()
  {
    // Arrange
    Contact contactToUpdate = new Contact(
        lastName = 'foo'
    );
    insert contactToUpdate;

    contactToUpdate.firstName='bar';

    ContactRepository contactRepository = new ContactRepository();

    // Act
    contactRepository.updateContacts(new List<Contact>{contactToUpdate});

    // Assert
    Contact updatedContact = contactRepository.getContact(contactToUpdate.Id);
    System.assertNotEquals(null,updatedContact.firstName);
  }
}