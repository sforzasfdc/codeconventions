public class ContactBuilder
{
  private Contact aContact;

  public ContactBuilder()
  {
    this.aContact = new Contact(
        LastName = 'foo'
    );
  }

  public ContactBuilder(Contact existingContact)
  {
    this.aContact = existingContact;
  }

  public ContactBuilder withId()
  {
    aContact.Id = fflib_IDGenerator.generate(Schema.Contact.SObjectType);
    return this;
  }

  public ContactBuilder withFavouriteMascotAstro()
  {
    aContact.favoriteMascot__c = 'Astro';
    return this;
  }

  public Contact build()
  {
    return aContact;
  }
}