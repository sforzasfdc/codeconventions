@isTest
private class OrganizationTypeTest {
    @isTest
    private static void testForOrgData() {
        // Arrange + Act
        String name = OrganizationType.getName();
        Boolean isSandbox = OrganizationType.isSandbox();
        String type = OrganizationType.getType();

        // Assert
        System.assertNotEquals(null, name);
        System.assertNotEquals(null, isSandbox);
        System.assertNotEquals(null, type);
    }
}
