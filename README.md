![Sforza](https://bitbucket.org/sforzasfdc/codeconventions/raw/master/img/sforza.jpg)

Published under [CC BY-SA](LICENSE).

# Sforza conventions for Salesforce

* [Dutch](https://bitbucket.org/sforzasfdc/codeconventions/raw/master/sfconventions.nl.pdf)
* [English](https://bitbucket.org/sforzasfdc/codeconventions/raw/master/sfconventions.en.pdf).