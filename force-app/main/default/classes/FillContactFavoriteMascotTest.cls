@IsTest(IsParallel=true)
private class FillContactFavoriteMascotTest
{
  @IsTest
  static void handle_shouldfillTheMascot()
  {
    //arrange
    FakeUserRepository userRepository = new FakeUserRepository();

    Profile profile = new ProfileBuilder()
        .withId()
        .build();

    User user = new UserBuilder()
        .withProfile(profile)
        .withId()
        .build();

    userRepository.insertUsers(new List<User>{user});

    Contact contact = new ContactBuilder()
        .build();

    //act
    new FillContactFavoriteMascot(
      userRepository,
      new List<Contact>{contact},
      user.Id
    ).handle();

    //assert
    System.assertNotEquals(null, contact.FavoriteMascot__c);
  }

  @IsTest static void handle_withPermissions_shouldNotfillTheMascot()
  {
    //arrange

    FakeUserRepository userRepository = new FakeUserRepository();

    Profile profile = new ProfileBuilder()
        .withId()
        .withPermissionsCustomizeApplication()
        .build();

    User user = new UserBuilder()
        .withProfile(profile)
        .withId()
        .build();

    userRepository.insertUsers(new List<User>{user});

    Contact contact = new ContactBuilder()
        .build();

    //act
    new FillContactFavoriteMascot(
      userRepository,
      new List<Contact>{contact},
      user.Id
    ).handle();

    //assert
    System.assertEquals(null, contact.FavoriteMascot__c);
  }
}