@isTest
public class TriggerBaseTest {
	
    private static testMethod void testTriggerBaseBeforeInsert(){
		new TriggerBase(TriggerBase.DmlOperations.DML_BEFORE_INSERT).processTrigger();
    }
    
	private static testMethod void testTriggerBaseAfterInsert(){
		new TriggerBase(TriggerBase.DmlOperations.DML_AFTER_INSERT).processTrigger();
    }    
    
    private static testMethod void testTriggerBaseBeforeUpdate(){
		new TriggerBase(TriggerBase.DmlOperations.DML_BEFORE_UPDATE).processTrigger();
    }
    
    private static testMethod void testTriggerBaseAfterUpdate(){
		new TriggerBase(TriggerBase.DmlOperations.DML_AFTER_UPDATE).processTrigger();
    }
    
    private static testMethod void testTriggerBaseBeforeDelete(){
		new TriggerBase(TriggerBase.DmlOperations.DML_BEFORE_DELETE).processTrigger();
    }
    
    private static testMethod void testTriggerBaseAfterDelete(){
		new TriggerBase(TriggerBase.DmlOperations.DML_AFTER_DELETE).processTrigger();
    }
    
    private static testMethod void testTriggerBaseAfterUndelete(){
		new TriggerBase(TriggerBase.DmlOperations.DML_AFTER_UNDELETE).processTrigger();
    }
}