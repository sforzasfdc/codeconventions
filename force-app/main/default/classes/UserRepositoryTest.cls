@IsTest
private class UserRepositoryTest
{
  @IsTest 
  static void getUser_shouldGetUser()
  {
    // Arrange
    String randomText = EncodingUtil.convertToHex(Crypto.generateAesKey(128)).substring(0,8);

    Profile sysAdminProfile = [
        SELECT Id
        FROM Profile
        WHERE PermissionsCustomizeApplication = TRUE
        LIMIT 1
    ];
    User testUser = new User(
        Alias = randomText,
        Email=randomText+'@example.com',
        EmailEncodingKey='UTF-8',
        LastName='Testing',
        LanguageLocaleKey='nl_NL',
        LocaleSidKey='nl_NL',
        TimeZoneSidKey='Europe/Amsterdam',
        Username='foo'+randomText+'@example.com',
        ProfileId = sysAdminProfile.Id
    );
    insert testUser;

    // Act
    User returnedUser = new UserRepository().getUser(testUser.Id);

    // Assert
    System.assertNotEquals(null, returnedUser);
  }
}