public interface IUserRepository
{
  User getUser(Id userId);
}