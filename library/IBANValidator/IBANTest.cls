@IsTest
private class IBANTest {
                @IsTest
    static void france()
    {
        System.assert(IBAN.validate('FR14 2004 1010 0505 0001 3M02 606')); // formated
        System.assert(IBAN.validate('FR7610807004090232158413487')); // no whitespaces
    }
    
    @IsTest
    static void malta()
    {
        System.assert(IBAN.validate('MT31MALT01100000000000000000123'));
    }

    @IsTest
    static void unitedKingdom()
    {
        System.assert(IBAN.validate('GB82 WEST 1234 5698 7654 32'));
    }
    
    @IsTest
    static void germany()
    {
        System.assert(IBAN.validate('DE89 3704 0044 0532 0130 00'));
    }
    
    @IsTest
    static void netherlands()
    {
        System.assert(IBAN.validate('NL02ABNA0123456789'));
    }
}
