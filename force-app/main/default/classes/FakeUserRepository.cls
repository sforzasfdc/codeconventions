public with sharing class FakeUserRepository implements IUserRepository
{
  List<User> users = new List<User>();

  public void insertUsers(List<User> users)
  {
    this.users.addAll(users);
  }

  public User getUser(Id userId)
  {
    for (User user : users)
    {
      if (user.Id == userId)
      {
        return user;
      }
    }
    return null;
  }
}