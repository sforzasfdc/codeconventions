public class OrganizationType {
    private static final Organization ORG_METADATA;

    static
    {
        ORG_METADATA = [
            SELECT  name, // dev4, uat, ...
                    isSandBox,
                    organizationType // Developer Edition, Enterprise Edition, ...
            FROM    Organization
            LIMIT   1
        ];
    }

    public static String getName()
    {
        return ORG_METADATA.name;
    }

    public static String getType()
    {
        return ORG_METADATA.organizationType;
    }

    public static Boolean isSandbox()
    {
        return ORG_METADATA.isSandbox;
    }
}