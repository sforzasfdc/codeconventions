public with sharing class ContactRepository implements IContactRepository
{
  public Contact getContact(Id contactId)
  {
    return[
        SELECT Id, lastName, firstName
        FROM Contact
        WHERE Id = :contactId
        LIMIT 1
    ];
  }

  public void insertContacts(List<Contact> contacts)
  {
    insert contacts;
  }

  public void updateContacts(List<Contact> contacts)
  {
    update contacts;
  }
}