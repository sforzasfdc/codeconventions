public with sharing class ContactTriggerHandler extends TriggerHandler {
  IUserRepository userRepository = new UserRepository();

  public override void beforeInsert(){
    new FillContactFavoriteMascot(
      userRepository,
      Trigger.new,
      UserInfo.getUserId()
    ).handle();
  }
}