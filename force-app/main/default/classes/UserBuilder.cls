@IsTest(IsParallel=true)
public class UserBuilder
{

  private User aUser;

  private static String randomText = EncodingUtil.convertToHex(Crypto.generateAesKey(128)).substring(0,8);

  public UserBuilder()
  {
    this.aUser = new User(
        Alias = randomText,
        Email=randomText+'@example.com',
        EmailEncodingKey='UTF-8',
        LastName='Testing',
        LanguageLocaleKey='nl_NL',
        LocaleSidKey='nl_NL',
        TimeZoneSidKey='Europe/Amsterdam',
        Username='foo'+randomText+'@example.com'
    );
  }

  public UserBuilder withId()
  {
    aUser.Id = fflib_IDGenerator.generate(Schema.User.SObjectType);
    return this;
  }

  public UserBuilder withProfile(Profile profile)
  {
    aUser.ProfileId = profile.Id;
    aUser.Profile = profile;
    return this;
  }

  public User build()
  {
    return aUser;
  }
}