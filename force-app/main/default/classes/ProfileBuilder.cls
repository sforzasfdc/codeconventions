@IsTest(IsParallel=true)
public class ProfileBuilder
{
  private Profile aProfile;

  public ProfileBuilder()
  {
    this.aProfile = new Profile(
        Name = 'foo'
    );
  }

  public ProfileBuilder withId()
  {
    aProfile.Id = fflib_IDGenerator.generate(Schema.Profile.SObjectType);
    return this;
  }

  public ProfileBuilder withPermissionsCustomizeApplication()
  {
    aProfile.PermissionsCustomizeApplication = true;
    return this;
  }

  public Profile build()
  {
    return aProfile;
  }
}